#!/home/zilker-technology/scripts/py2.7playground/bin/python
"""author: Marko Petrovic<mpetrovic@ztech.rs>"""

# import logging
import send_to_graylog
import socket
import shutil
import subprocess
import gzip
import smtplib
import requests
import time
import imaplib
import email
import os
import calendar
# from calendar import monthrange
import urllib
from datetime import datetime, timedelta
from os import path
import io
import zipfile
try:
    import creds
    import wget
    from bs4 import BeautifulSoup
except ImportError as IE:
    print(IE)
    print
    print("Install with: pip install wget/BeautifulSoup")
    exit()



ORG_EMAIL   = creds.ORG_EMAIL
FROM_EMAIL  = creds.FROM_EMAIL  #+ ORG_EMAIL
FROM_PWD    = creds.FROM_PWD
SMTP_SERVER = creds.SMTP_SERVER
SMTP_PORT   = creds.SMTP_PORT

os.chdir(os.path.dirname(os.path.realpath(__file__)))

# MAIN FN
def main():
    
    for file in os.listdir("."):
        if file.endswith(".zip"):
          print("zip file found \n Starting Unzip.. \n")
          try:
            unzip_file()

            forward_to_graylog()

            perform_cleanup()
          except:
              print("Something failed")
        else:
            pass
    
     
    # day_of_the_week = datetime.today().day -1
    yesterday = str((datetime.now()- timedelta(days=1)).strftime('%d-%h-20%y'))
    current_month = str(datetime.now().strftime('%h'))
    current_year = str(datetime.today().year)

    try:
        print("Connecting to server....")
        mail = imaplib.IMAP4_SSL(SMTP_SERVER)
        mail.login(FROM_EMAIL,FROM_PWD)
        print("Connected")
        mail.select('inbox')
        type, data = mail.search(None, '(SINCE '+ yesterday +')')
        mail_ids = data[0]
        id_list = mail_ids.split()  
        # first_email_id = id_list[0]
        latest_email_id = id_list[-1]
        memorized = int(read_last_time())

        if memorized is 1 or None:
            memorized = int(latest_email_id) - 10

        if memorized != int(latest_email_id):
            write_time(latest_email_id)
            for i in range(int(latest_email_id),int(memorized), -1):
                typ, msg_data = mail.fetch(str.encode(str(i)), "(RFC822)")

                for response_part in msg_data:
                    if isinstance(response_part, tuple):
                        msg = email.message_from_string(response_part[1].decode())
                        email_subject = msg['subject']
                        email_from = msg['from']
                        if email_from == 'noreply@alienvault.com' and email_subject == 'USM Anywhere Raw Log File Request':
                            if msg.is_multipart():
                                for payload in msg.get_payload():
                                        print(payload)
                            else:
                                body = msg.get_payload(decode=True)
                                soup = BeautifulSoup(body, features="html.parser")
                                links = soup.find_all("a")
                                for tag in links:
                                    link = tag.get('href', None)
                                    if link.__contains__("amazon"):
                                        if download_zip(link) == 1:
                                            break
            
            write_time(latest_email_id)

            unzip_file()
            
            forward_to_graylog()
            
            perform_cleanup()
 
        else:
            print("New email not found")
            print("Exiting")
    except Exception as ex:
        print("Here 1")
        print(ex)
        raise


def read_last_time():
    try:
       date_time = open("date.txt", "r")
       if date_time.mode == "r":
          number = date_time.read()
          date_time.close()
          return number
    except Exception as ex:
        os.system("echo 1\n > date.txt")
        return read_last_time()
        

def write_time(email_time):
    try:
        with open("date.txt", "w") as date_time:
            date_time.write(email_time)
    except Exception as ex
        raise ex


def download_zip(url):
    try:
        zip_file = wget.download(url)
        try:
            test = zipfile.ZipFile(zip_file).testzip()
            if test is None:
                print("ZIP-TEST: PASSED")
                # unzip_file()
        except:
            print("ZIP-TEST: FAILED")
            os.remove(zip_file)
            return 1
    except:
        # logger.exception()
        raise


def unzip_file():
    counter = 0
    for file in os.listdir("."):
        if file.endswith(".zip"):
            counter += 1
            try:
                with zipfile.ZipFile(path.abspath(file), 'r') as zip_ref:
                        zip_ref.extractall( "./"+str(datetime.now()))
            except:
                break
    # force log rotation
    if counter > 1:
         print("More than 2 zips downloaded..\n rotating.....")
         requests.post("http://127.0.0.1:9000/api/system/deflector/cycle", auth=("devops","lozinkazarotate"), verify=False)
         print("\n Logs rotated. \n")
    time.sleep(1)
    execute_control(file)

def execute_control(file):
    for dir in os.listdir("."):
        if path.isdir(dir):
            for i in os.listdir("./"+dir):
                unzip_gzip(dir+"/"+i)


def unzip_gzip(file):
    with gzip.open(file, 'r') as log_file:
        read = log_file.read()
        new_file = file[:len(file)-3]
        with open(new_file,'w') as final_file:
            final_file.write(read)


def forward_to_graylog():
    HOST ="127.0.0.1"
    SPORT = 514
    GPORT = 12201

    ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sg = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    ss.connect((HOST, SPORT))
    sg.connect((HOST, GPORT))

    for dir in os.listdir("."):
        if path.isdir(dir):
            for i in os.listdir(dir):
                if i.endswith(".gz"):
                    pass
                else:
                    print("Forwarding..\n FILE: "+ i)
                    send_to_graylog.main(dir+"/"+i, ss, sg)
    ss.close()
    sg.close()      


def perform_cleanup():
    os.chdir("..")
    try:
        if "copied_zip" not in os.listdir("."):
            os.makedirs("copied_zip", 0755)
        os.chdir("./alien_checker")
        for file in os.listdir("."):
            if file.endswith(".zip"):
                shutil.move(file, "../../archive")
        for dir in os.listdir("."):
            if path.isdir(dir):
               shutil.rmtree(dir)
    except:
        continue    
    

# ENTRY
if __name__ == '__main__':
	main()
