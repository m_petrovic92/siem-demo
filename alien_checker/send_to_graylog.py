#!/bin/python

import re
import socket
import os
import logging
from logging.handlers import SysLogHandler
import sys
import json
import syslog
import base64
import time


# main fn
def main(filelog, ss, sg):
    try:
        parse_file(filelog, ss, sg)
    except Exception as exp:
        print(exp)

def parse_file(filelog, ss, sg):
    try:
        with open(filelog) as log:
            for line in log.readlines():
                if re.search("<.*>", line):
                    socker_connect_tcp_s(line, ss)
                    #print "sockernum1"
                    time.sleep(.000001)
                elif re.search("^http ", line) or re.search("^https ", line) or re.search("^h2 ", line) or re.search("^[a-zA-Z0-9.:-]* ", line):
                    try:
                        syslog.syslog((syslog.LOG_LOCAL1|syslog.LOG_INFO), line)
                        time.sleep(.000001)
                        #print "syslog1num"
                    except:
                        syslog.syslog(syslog.LOG_INFO, "AlienVaultBase64: " + base64.b64encode(line))
                        time.sleep(.000001)
                        print "syslog1num2"
                else:
                    try:
                        data = json.loads(line)
                        syslog.syslog((syslog.LOG_LOCAL1|syslog.LOG_INFO), line)
                        time.sleep(.000001)
                            #print "syslog2num"
                    except ValueError:
                        socker_connect_tcp_g(format_gelf_json(line), sg)
                        time.sleep(.000001)
                            #print "sockernum2"
                    except TypeError as te:
                        print te

    except IOError as FileExeption:
                print FileExeption


# send line as base64 encoded string to syslog
def b64encode(line):

    return base64.b64encode(line)

# def socker_connect(data):
#     try:
#         s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 
#         s.sendto(bytes(data), (HOST, PORT))
#     except socket.error as se:
#         print se

def format_gelf_json(input):
    return json.dumps(
        {
         "version": "1.1",
         "host": "zilker-technology.gelf",
         "short_message": "AlienVaultStored:" +  b64encode(input),
         "full_message": str(input) ,
         "timestamp": int(time.time()),
         "level": 1
        }) + "\0"

# send packages over TCP
def socker_connect_tcp_s(data, ss):
    ss.send(data.encode('utf-8'))

def socker_connect_tcp_g(data, sg):
    sg.send(data.encode('utf-8'))


# ENTRY
# if __name__ == "__main__":
#     print "********** STARTING ***********"
#     print "*========== WORKING ==========*"
#     main()
#     print "********** DONE ***************"

