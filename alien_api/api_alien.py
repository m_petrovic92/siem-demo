#!/home/zilker-technology/scripts/py3playground/bin/python

import json
import urllib3
import os
import socket
import os
import datetime
import time
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders


def install_requriments():
    input_string = input("You do not have requests module installed\n Do you want to install it? <y/n>")
    if input_string == "y":
        try:
            os.system("pip install requests")
        except:
            input_pip = input("pip is also not installed, do you want to install it? (y/n)")
    else:
        os._exit(1)

    if input_pip == "y":
        try:
            os.system( 'curl "https://bootstrap.pypa.io/get-pip.py" -o "get-pip.py"')
            os.system('python get-pip.py')
        except:
            print ("Installation failed, try with sudo or manually")
    else:
        os._exit(1)


try:
    import requests
except ImportError as IE:
    install_requriments()
    
            
urllib3.disable_warnings()

HOST ="127.0.0.1"
PORT = 12201

alarms_url = "https://zilker-technology.alienvault.cloud/api/2.0/alarms/"


os.chdir(os.path.dirname(os.path.realpath(__file__)))


# main function
def main():
    test = make_req(alarms_url)['_embedded']['alarms']
    uuidList = []
    for i in test:
        uuidList.append(i['uuid'])
        # print type(i['uuid'])
    
    read_file(uuidList)

# send packages over TCP
def socker_connect_tcp(data):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    s.send(data.encode('utf-8'))
    s.close()
    print ("SENT TCP")


def create_open_file(name):
    with open(name, "a+"):
        return name


def read_file(uuid):
    first = uuid[0]
    file_name = create_open_file("alien-uuid.txt")
    with open(file_name, 'r') as f:
        for lines in f.readlines():
            # tip = unicode(lines.strip(), "utf-8")
            tip = lines.strip()
            if tip == first:
                print (tip)
                print (first)
                print ("Here first")
                break
            # ako nije,upis i nadji joj poziciju u najnovijoj listi i od nje posalji response za svaki id koji dolazi nakon 
            elif tip != first:
                write_to_file(file_name, first)
                if tip in uuid:
                    for i in uuid[:uuid.index(tip)]:
                        print ("Here sock")
                        socker_connect_tcp(format_gelf_json(make_req(alarms_url+i), i))   

                else:
                    write_to_file(file_name, first)
                    # send_mail("More then 20 new alerts were catched(page1)")
                    for i in uuid:
                        print ("Here sock1")
                        socker_connect_tcp(format_gelf_json(make_req(alarms_url+i), i))
            else:
                break

def write_to_file(file_name, uuid):
    with open(file_name, 'w+') as f:
        f.write(uuid)
        # print >> f,uuid
        return

# format to gelf json
def format_gelf_json(input, uuid):
    return json.dumps(
        {
         "version": "1.1",
         "host": "zilker-technology.alienvault.cloud",
         "short_message": "AlienVault alarm: " + uuid,
         "full_message": str(input) ,
         "timestamp": int(time.time()),
         "level": 1
        }) + "\0"


def get_token():
    r = requests.post("https://zilker-technology.alienvault.cloud/api/2.0/oauth/token?grant_type=client_credentials", auth=('zilkeraja', 'i93jhJ9m5jbQfySJ9WBh2oaC4u33jrKL'), verify=False)
    return r.json()["access_token"]


def make_req(urlA):
    bearer = get_token()
    headers = {'Authorization': 'bearer '+ bearer, 'accept': 'application/json'}
    r = requests.get(urlA, headers=headers)
    # jsonr = json.loads(r.content)
    jsonr = r.json()
    return jsonr


# send email
def send_mail(text):
    emailList = ["giga@siem1.ztech.rs"]#, "mpetrovic@ztech.rs"]
    sender = socket.gethostname()
    msg = MIMEMultipart()
    msg['Subject'] = 'Alien-alarm-parser'
    msg['From'] = sender
    msg['To'] = ' , '.join(emailList)
    msg.attach(MIMEText(text, 'plain'))
    try:
        s = smtplib.SMTP("127.0.0.1", 25)
        s.set_debuglevel(1)
        # s.login("", "")
        s.sendmail(sender, emailList, msg.as_string())
        s.quit()
    except smtplib.SMTPException as sme:
        print (sme)

# ENTRY
if __name__ == "__main__":
    main()
